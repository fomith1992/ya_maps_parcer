/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

export interface TService {
  id: string;
  title: string;
  category_id: number;
  price_min: number;
  price_max: number;
  discount: number;
  prepaid: string;
}
