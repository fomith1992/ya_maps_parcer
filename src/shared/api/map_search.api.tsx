import { ya_instance } from "./base";

const API_KEY = "237600b2-f022-49be-bd98-628abff8b096";

export interface Organization {
  type: "Feature";
  geometry: {
    type: "Point";
    coordinates: [30.371705, 59.935739];
  };
  properties: {
    name: string;
    description: string;
    boundedBy: [[30.367611, 59.933861], [30.375822, 59.937982]];
    CompanyMetaData: {
      id: string;
      name: string;
      address: string;
      url: string;
      Phones: {
        type: string;
        formatted: string;
      }[];
      Categories: [
        {
          class: string;
          name: string;
        },
        {
          class: string;
          name: string;
        }
      ];
      Hours: {
        text: string;
        Availabilities: [
          {
            Intervals: [
              {
                from: string;
                to: string;
              }
            ];
            Everyday: true;
          }
        ];
      };
    };
  };
}

interface TSearchData {
  type: "FeatureCollection";
  properties: {
    ResponseMetaData: {
      SearchResponse: {
        found: 13;
        display: "multiple";
      };
      SearchRequest: {
        request: string;
        skip: 0;
        results: 10;
        boundedBy: [[37.048427, 55.43644866], [38.175903, 56.04690174]];
      };
    };
  };
  features: Organization[];
}

export const handleSearch = async (data: { query: string }) =>
  await ya_instance.get<TSearchData>(`?apikey=${API_KEY}&text=${data.query}&lang=ru_RU&results=500`);
