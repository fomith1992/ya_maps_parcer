import axios from "axios";

const API_PREFIX = "https://search-maps.yandex.ru/v1/";

export const ya_instance = axios.create({
  baseURL: API_PREFIX,
});
