/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

export type TZone = {
  categoryName: string;
  text: string;
  price: number;
  price_card: number;
  id: number;
  id_card: number;
};

export type TSection = {
  title: string;
  data: TZone[];
};

export type THeadersTitles = {
  text: string;
  id: number;
};

export type ViewToken = {
  item: TZone;
  key: string;
  index: number | null;
  isViewable: boolean;
  section?: TSection;
};
