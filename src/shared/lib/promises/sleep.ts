/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

export function sleep(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}
