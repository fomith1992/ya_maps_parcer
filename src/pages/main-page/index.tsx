/*
 * Dmitry Fomenko
 * shaiyaby@yandex.by
 * For Missis Laser, 2022
 */

import React, { useState } from "react";
import { Badge, Button, Input, Table, Tag, Typography } from "antd";
import useAsyncFn from "react-use/lib/useAsyncFn";
import { handleSearch, Organization } from "../../shared/api/map_search.api";
import { CSVLink } from "react-csv";
import { DownloadOutlined, ForwardOutlined } from "@ant-design/icons";
import { useExcelDownloder } from "react-xls";

const { Search } = Input;

const columns = [
  {
    title: "Ссылка",
    dataIndex: "link",
    key: "link",
    render: (text: string) => (
      <a href={text} target='_blank' rel='noreferrer'>
        {text}
      </a>
    ),
  },
  {
    title: "Название",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "адрес",
    dataIndex: "address",
    key: "address",
  },
];

export const MainPage = (): React.ReactElement => {
  const [storyData, setStoryData] = useState<{ [key: React.Key]: Organization }>({});
  const { ExcelDownloder, Type } = useExcelDownloder();
  const [visible, isVisible] = useState<boolean>(false);

  const [data, setdata] = useState<Organization[]>([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  console.log(selectedRowKeys.length);

  const [, onSaveDataToFile] = useAsyncFn(async () => {
    const localData = storyData;
    selectedRowKeys.forEach(x => {
      const item = data.find(
        y => y?.properties?.CompanyMetaData?.url && y?.properties?.CompanyMetaData?.url.split("?")[0] === x
      );
      if (item) {
        localData[x] = item;
      }
    });

    setStoryData(localData);
  }, [storyData, selectedRowKeys, data]);

  const [, onSearch] = useAsyncFn(async (query: string) => {
    await handleSearch({ query }).then(res => {
      setdata(
        res.data.features && res.data.features.length > 0
          ? res.data.features.filter(x => x.properties.CompanyMetaData.url)
          : []
      );
      setSelectedRowKeys([]);
    });
  }, []);

  const [, onSelectChange] = useAsyncFn(
    async (newSelectedRowKeys: React.Key[]) => {
      setSelectedRowKeys(newSelectedRowKeys);
    },
    [selectedRowKeys]
  );

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  setInterval(() => {
    console.log("object");
  }, 1000);

  return (
    <div style={{ position: "relative", overflow: "hidden" }}>
      <div style={{ position: "relative", display: "flex", flexDirection: "column", alignItems: "center" }}>
        <Typography
          style={{
            fontSize: 16,
            fontFamily: "sans-serif",
            textAlign: "center",
            marginTop: "4px",
            marginBottom: "16px",
            fontWeight: 400,
          }}
        >
          Dmitry Fomenko
        </Typography>
        <Typography
          style={{
            fontSize: 40,
            fontFamily: "sans-serif",
            textAlign: "center",
            marginBottom: "16px",
            fontWeight: 800,
          }}
        >
          Super YA MAP parser
        </Typography>

        <Search
          style={{ width: "400px" }}
          placeholder='input search text'
          onSearch={onSearch}
          // value='парикмахерская Минск' //! удвлить параметр
          enterButton
        />
        <div style={{ marginTop: "16px" }}>
          Выбрано организаций: <Tag color='red'>{selectedRowKeys.length}</Tag>
          <Badge count={Object.keys(storyData).length} showZero>
            <Button onClick={() => onSaveDataToFile()} type='primary'>
              Добавить в файлs
            </Button>
          </Badge>
          <CSVLink
            title='ya-map-companies'
            filename='ya-map-companies'
            data={Object.keys(storyData).map(x => ({
              id: storyData[x].properties.CompanyMetaData.id,
              link: storyData[x].properties.CompanyMetaData.url.split("?")[0],
              name: storyData[x].properties.CompanyMetaData.name,
              address: storyData[x].properties.CompanyMetaData.address,
              phones:
                storyData[x].properties.CompanyMetaData.Phones &&
                storyData[x].properties.CompanyMetaData.Phones.map(x => x.formatted),
            }))}
          >
            <Button style={{ marginLeft: "24px" }} type='primary' icon={<DownloadOutlined />} size={"middle"} />
          </CSVLink>
          {!visible && (
            <Button
              style={{ marginLeft: "24px" }}
              type='primary'
              icon={<ForwardOutlined />}
              onClick={() => isVisible(true)}
              size={"middle"}
            >
              EXEL
            </Button>
          )}
          {visible && (
            <ExcelDownloder
              data={{
                animals: [
                  ...Object.keys(storyData).map(x => ({
                    id: storyData[x].properties.CompanyMetaData.id,
                    link: storyData[x].properties.CompanyMetaData.url.split("?")[0],
                    name: storyData[x].properties.CompanyMetaData.name,
                    address: storyData[x].properties.CompanyMetaData.address,
                    phone1: storyData[x].properties.CompanyMetaData.Phones
                      ? storyData[x].properties.CompanyMetaData.Phones[0]?.formatted
                      : "",
                    phone2: storyData[x].properties.CompanyMetaData.Phones
                      ? storyData[x].properties.CompanyMetaData.Phones[1]?.formatted
                      : "",
                    phone3: storyData[x].properties.CompanyMetaData.Phones
                      ? storyData[x].properties.CompanyMetaData.Phones[2]?.formatted
                      : "",
                    phone4: storyData[x].properties.CompanyMetaData.Phones
                      ? storyData[x].properties.CompanyMetaData.Phones[3]?.formatted
                      : "",
                    phone5: storyData[x].properties.CompanyMetaData.Phones
                      ? storyData[x].properties.CompanyMetaData.Phones[4]?.formatted
                      : "",
                  })),
                ],
              }}
              filename={"book"}
              type={Type.Link}
            >
              <Button
                style={{ marginLeft: "24px" }}
                type='primary'
                icon={<DownloadOutlined />}
                onClick={() => isVisible(false)}
                size={"middle"}
              >
                Download
              </Button>
            </ExcelDownloder>
          )}
        </div>
        <Table
          style={{ marginTop: "24px", maxWidth: "1360px" }}
          dataSource={
            data && data.length > 0
              ? data.map(item => ({
                  key: item?.properties?.CompanyMetaData?.url?.split("?")[0] ?? null,
                  link: item?.properties?.CompanyMetaData?.url?.split("?")[0] ?? null,
                  name: item?.properties?.CompanyMetaData?.name,
                  address: item?.properties?.CompanyMetaData?.address,
                }))
              : []
          }
          columns={columns}
          pagination={false}
          rowSelection={rowSelection}
        />
      </div>
    </div>
  );
};
